#include <SDL2/SDL_render.h>
#include <iostream>
#include <fstream>
#include <string>
#include <strings.h>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

int main(int argc, char** argv) {
	// only 1 arg for now
	if(argc != 3) {
		std::cerr<<"Usage: "<<argv[0]<<" <font> <filename>"<<std::endl;
		return 1;
	}

	std::vector<std::string> buffer;
	std::string tmp, tmp2;
	// check the file
	std::fstream file(argv[2], std::ios::in);
	if(file) {
		// read the file
		while(std::getline(file, tmp)) {
			tmp2.clear();
			if(tmp.empty()) continue;
			for(char c : tmp) {
				switch(c) {
					case '\t':
						tmp2 += "    ";
						break;
					default:
						tmp2.push_back(c);
						break;
				}
			}
			buffer.push_back(tmp2);
		}
		file.close();
	} else {
		buffer.push_back("Roses are red,");
		buffer.push_back("Violets are blue,");
		buffer.push_back("But someone like you...");
		buffer.push_back("Belongs in the zoo.");
		buffer.push_back("However, dont worry,");
		buffer.push_back("I'll be there too!");
		buffer.push_back("Outside the cage,");
		buffer.push_back("And laughing at you!");
	}
	tmp.clear();
	tmp2.clear();

	// init sdl
	SDL_Init(SDL_INIT_VIDEO);
	SDL_Window* window = SDL_CreateWindow("Wedit", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1920, 1080, SDL_WINDOW_RESIZABLE);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	TTF_Init();

	// some vars
	int x = 0, y = 0, w = 0, h = 0, win_w = 0, win_h = 0, fontsize = 24, i = 0, sx = 0, sy = 0, ctrlt = 1;

	std::string clipboard = "";
	
	bool quit = 0;
	SDL_Event event;

	// load font
	TTF_Font* font = TTF_OpenFont(argv[1], fontsize);

	// set color
	SDL_Color text_color = {0xae, 0xae, 0xae, 0xff};

	while(!quit) {
		SDL_SetRenderDrawColor(renderer, 0x11, 0x11, 0x11, 0xff);
		SDL_RenderClear(renderer);

		SDL_GetWindowSize(window, &win_w, &win_h);

		while(SDL_PollEvent(&event)) {
			switch(event.type) {
				case SDL_QUIT:
					quit = 1;
					break;
				case SDL_TEXTINPUT:
					TTF_SizeUTF8(font, event.text.text, &w, &h);
					i = (win_h / 2 - y) / h;
					if(i >= (int)buffer.size()) break;
					else if(i < 0) break; //should never happen
                    for (const char *ptr = event.text.text; *ptr; ptr++) {
    					buffer.at(i).push_back(*ptr);
                    }
					x -= w;
					break;
				case SDL_WINDOWEVENT:
					if(event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
						SDL_GetRendererOutputSize(renderer, &event.window.data1, &event.window.data2);
					}
			}
		}

		const Uint8* keyboardState = SDL_GetKeyboardState(nullptr);
		if(keyboardState[SDL_SCANCODE_RIGHT]) {
			sx -= 3;
		}
		if(keyboardState[SDL_SCANCODE_LEFT]) {
			sx += 3;
		}
		if(keyboardState[SDL_SCANCODE_UP]) {
			sy += 3;
		}
		if(keyboardState[SDL_SCANCODE_DOWN]) {
			sy -= 3;
		}
		if(keyboardState[SDL_SCANCODE_BACKSPACE]) {
			TTF_SizeUTF8(font, std::string(" ").c_str(), &w, &h);
			i = (win_h / 2 - y) / h;
			if(i < (int)buffer.size() && i >= 0 && buffer.at(i).size() && !buffer.empty() && i) {
				buffer.at(i).pop_back();
				x += w;
			}
		}
		if(keyboardState[SDL_SCANCODE_LCTRL] && !ctrlt) {
			ctrlt = 5;
			if(keyboardState[SDL_SCANCODE_S]) {
				std::ofstream out(argv[2]);
				if(!out) {
					std::cerr<<"Unable to open \""<<argv[2]<<"\""<<std::endl;
				} else {
					for(std::string line : buffer) {
						if(line.empty()) continue;
						out << line << std::endl;
					}
				}
				out.close();
			} else if(keyboardState[SDL_SCANCODE_R]) {
				sx = 0;
				sy = 0;
				x = 0;
				y = 0;
			} else if(keyboardState[SDL_SCANCODE_N]) {
				i = (win_h / 2 - y) / h;
				if(i < (int)buffer.size() && i >= 0 && buffer.at(i).size()) {
					buffer.insert(buffer.begin() + i + 1, "...");
				}
			} else if(keyboardState[SDL_SCANCODE_V]) {
				i = (win_h / 2 - y) / h;
				if(i < (int)buffer.size() && i >= 0 && buffer.at(i).size()) {
					buffer.at(i) = clipboard;
				}
			} else if(keyboardState[SDL_SCANCODE_C]) {
				i = (win_h / 2 - y) / h;
				if(i < (int)buffer.size() && i >= 0 && buffer.at(i).size()) {
					clipboard = buffer.at(i);
				}
			} else if(keyboardState[SDL_SCANCODE_X]) {
				if(buffer.size() > 1) {
					i = (win_h / 2 - y) / h;
					if(i < (int)buffer.size() && i >= 0 && buffer.at(i).size()) {
						clipboard = buffer.at(i);
						buffer.erase(buffer.begin() + i);
					}
				} else if(buffer.size()){
					clipboard = buffer[0];
				}
			} else if(keyboardState[SDL_SCANCODE_D]) {
				if(buffer.size() > 1) {
					i = (win_h / 2 - y) / h;
					if(i < (int)buffer.size() && i >= 0 && buffer.at(i).size()) {
						buffer.erase(buffer.begin() + i);
					}
				}
			} else {
				ctrlt = 0;
			}
		}
		if(ctrlt) ctrlt--;

		if(buffer.at(buffer.size() - 1).empty()) buffer.pop_back();

		// our cursor
		SDL_SetRenderDrawColor(renderer, 0xff, 0, 0, 0xff);
		SDL_RenderDrawLine(renderer, win_w, win_h / 2 + h, 0, win_h / 2 + h);

		x += sx;
		y += sy;
		if(sx > 0) sx--;
		else if(sx < 0) sx++;
		if(sy > 0) sy--;
		else if(sy < 0) sy++;

		i = 0;
		/* std::cout<<"\033[2J\033[H"<<std::endl; */
		for(std::string line : buffer) {
			TTF_SizeUTF8(font, line.c_str(), &w, &h);
			i++;
			if(x > win_w) continue;
			if(i * h + y + h < 0) continue;
			if(i * h + y > win_h) break;
			SDL_Rect line_rect;
			line_rect.x = x;
			line_rect.y = i * h + y;
			line_rect.w = w;
			line_rect.h = h;
			SDL_Surface *surface = TTF_RenderUTF8_Solid(font, line.c_str(), text_color);
			SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
			SDL_RenderCopy(renderer, texture, NULL, &line_rect);
			SDL_FreeSurface(surface);
			SDL_DestroyTexture(texture);
		}

		SDL_RenderPresent(renderer);
	}

	TTF_Quit();
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}
